#include <avr/io.h>
#include <util/delay.h> 
#include <avr/interrupt.h>



int main (void){
      sei();
    unsigned char result;
    DDRB = 0xFF; 
    ADMUX = 0b00100000;
      ADCSRA = 0b10000111;
    while(1){
    ADCSRA |= 1<<ADSC;
    while(ADCSRA & (1 << ADSC)){}
    result =ADCH; 
    PORTD = result;
    }
    return 0;
}

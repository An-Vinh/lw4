#include <avr/io.h>
#include <util/delay.h> 
#include <avr/interrupt.h>




#include<avr/io.h>
  

/*
int main (void){
      sei();
    unsigned char result;
    DDRB = 0xFF; 
    ADMUX = 0b00100000;
      ADCSRA = 0b10000111;
    while(1){
    ADCSRA |= 1<<ADSC;
    while(ADCSRA & (1 << ADSC)){}
    result =ADCH; 
    PORTD = result;
    }
    return 0;
}*/



volatile char result;
int main (void){
    
  Serial.begin(9800);
    DDRB = 0xFF; 
    ADMUX = 0b00100011;
      ADCSRA = 0b10001110;
      sei();
      while(1){
      ADCSRA |= (1<<ADSC);
      PORTD = result;
    }
      
    return 0;
}

ISR(ADC_vect){
 
    result = ADCH; 
  	Serial.print(result);
}